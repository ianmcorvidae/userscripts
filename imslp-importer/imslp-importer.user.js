// ==UserScript==
// @name        MusicBrainz: Import works from IMSLP
// @version     2014-09-18.0.2
// @author      ianmcorvidae
// @namespace   http://ianmcorvidae.net/
//
// @include     *://imslp.org/wiki/*
// ==/UserScript==
//**************************************************************************//

var myform = document.createElement("form");
myform.method = "get";
myform.action = "//beta.musicbrainz.org/work/create";
myform.acceptCharset = "UTF-8";
mysubmit = document.createElement("input");
mysubmit.type = "submit";
mysubmit.value = "Add to MusicBrainz";
myform.appendChild(mysubmit);

var div = document.createElement("div");
div.style.position = 'absolute';
div.style.top = 0;
div.style.right = 0;
div.style.padding = '20px';
div.style.margin = '50px';
div.style.zIndex = '10000';

imslp_setup();

function imslp_setup() {
    var info_header = document.querySelector('#General_Information');
    if (info_header) {
        var head_container = info_header.parentNode.nextElementSibling.firstElementChild;
        var body_container = info_header.parentNode.nextElementSibling.nextElementSibling.firstElementChild;

        var title;
        var edit_note = "Imported from " + document.location.href + "\n\n";
        edit_note = edit_note + "Information in IMSLP's information table:\n";

        var process_table = function (table) {
            if (table.tagName === "TABLE") {
                for (var rn = 0, row; row = table.rows[rn]; rn++) {
                    if (row.cells[0].textContent.trim() === "Work Title") {
                        title = row.cells[1].textContent.trim();
                    } else if (row.cells[1].textContent.trim() !== "") {
                        edit_note = edit_note + row.cells[0].textContent.trim() + ": " + row.cells[1].textContent.trim() + '\n';
                    }
                }
            }
        };

        process_table(head_container);
        process_table(body_container);

        add_field("edit-work.name", title);
        add_field("edit-work.edit_note", edit_note);
        add_field("edit-work.url.0.link_type_id", "274");
        add_field("edit-work.url.0.text", document.location.href);

        var mb_ws_url = "//musicbrainz.org/ws/2/url/?inc=work-rels&fmt=json&resource=" + fix_url(document.location.href);

        var xmlhttp2 = new XMLHttpRequest();
        xmlhttp2.open('GET', mb_ws_url, true);
        xmlhttp2.onreadystatechange = function() { mb_singlepage_callback(xmlhttp2); }
        xmlhttp2.send(null);
    }

    var category_page_links = document.querySelectorAll('.categorypagelink')
    if (category_page_links.length) {
        for (var ei = 0, element; element = category_page_links[ei]; ei++) {
            (function (element, ei) { // closure over element, ei, span
                var span = document.createElement('span');
                span.style.minWidth = '20px';
                span.style.float = 'right';
                span.innerHTML = '<img src="//musicbrainz.org/static/images/icons/loading.gif" />';

                window.setTimeout(function () {
                    var mb_ws_url = "//musicbrainz.org/ws/2/url/?inc=work-rels&fmt=json&resource=" + fix_url(element.href);
                    var xmlhttp2 = new XMLHttpRequest();
                    xmlhttp2.open('GET', mb_ws_url, true);
                    xmlhttp2.onreadystatechange = function() { mb_link_callback(xmlhttp2, span, element.href); }
                    xmlhttp2.send(null);
                }, ei * 1000);
                element.parentNode.appendChild(span);
            })(element, ei);
        }
    }
}

function mb_singlepage_callback(req) {
    if (req.readyState != 4)
        return;
    var r = eval('(' + req.responseText + ')');

    if (r.relations) {
        div.style.backgroundColor = 'lightgreen';
        div.innerHTML = "<a href='//musicbrainz.org/url/" + r.id + "'>Already in MB</a> :D";
        document.body.appendChild(div);
    } else {
        div.style.backgroundColor = 'pink';
        div.appendChild(myform);
    }

    document.body.appendChild(div);
}

function mb_link_callback(req, span, fallback) {
    if (req.readyState != 4)
        return;

    var r = eval('(' + req.responseText + ')');

    if (r.relations) {
        span.style.backgroundColor = 'lightgreen';
        span.innerHTML = "<a href='//musicbrainz.org/url/" + r.id + "'>MB</a>";
    } else {
        span.style.backgroundColor = 'pink';
        span.innerHTML = "<a href='" + fallback + "'>&mdash;</a>";
    }

}

function add_field (name, value) {
    var field = document.createElement("input");
    field.type = "hidden";
    field.name = name;
    field.value = value;
    myform.appendChild(field);
}

function fix_url (url) {
    return encodeURI(url.replace('%27', "'"))
}
